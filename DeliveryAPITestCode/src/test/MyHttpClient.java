package test;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;

public class MyHttpClient{ 
	
	public CloseableHttpClient getNewHttpClient(boolean production) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
		if(production){
			
			SSLContext context = SSLContexts.createDefault();
			HttpClientBuilder builder = HttpClientBuilder.create();
		    SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(context);
		    builder.setSSLSocketFactory(sslConnectionFactory);
	
		    Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
		            .register("https", sslConnectionFactory)
		            .build();
		    HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
		    builder.setConnectionManager(ccm);
	
		    return builder.build();
		    
		}else{
			
			TrustManager[] tm = new TrustManager[1];
			tm[0] = new X509TrustManager() {
	            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
	
	            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }
	
	            public X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	        };
			
			SSLContext context = SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build();
			context.init(null, tm, null);
			HttpClientBuilder builder = HttpClientBuilder.create();
		    SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(context, new NoopHostnameVerifier());
		    builder.setSSLSocketFactory(sslConnectionFactory);
	
		    Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
		            .register("https", sslConnectionFactory)
		            .build();
	
		    HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
		    builder.setConnectionManager(ccm);
		    return builder.build();
		}
	}
}
