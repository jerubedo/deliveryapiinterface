package test;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

public class PostDeliveryData{	


	  public static void main(String[] args) throws Exception {
		  
		MyHttpClient client = new MyHttpClient();
		//CloseableHttpClient httpclient = client.getNewHttpClient(false);//Staging use ONLY, accepts ALL certificates
		CloseableHttpClient httpclient = client.getNewHttpClient(true);//Production use

	   ////////////Upload multiple deliveries using one POST request
	    HttpPost httppost = new HttpPost("https://delivery.bloomnet.net/DeliveryAPIBeta2/v1/InsertOrUpdateDeliveryInfoInBulk");
     String jsonString = ""
       + "[{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN1\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2828\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN3\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2930\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"391 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2931\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"392 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2932\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"393 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2933\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"394 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2934\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"395 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2935\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"396 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2936\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"397 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2937\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"398 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2938\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"399 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2939\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"390 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2940\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3900 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2941\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3911 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2942\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3922 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2943\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3933 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2944\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3944 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2945\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3955 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2946\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3966 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2947\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3977 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2948\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3988 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN2949\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"3999 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"}]";

	    HttpEntity httpEnt = MultipartEntityBuilder.create()
	    		.addPart("apiKey", new StringBody("mf$Sf3m!tkdsZkf5nP(7dnsD$1co9DGjNsf2niCn$0vUf(I9em", ContentType.TEXT_PLAIN))
	    		.addPart("jsonArray", new StringBody(jsonString, ContentType.TEXT_PLAIN))
	    		.build();
	    httppost.setEntity(httpEnt);
	    
	    System.out.println("executing request " + httppost.getRequestLine());
	    long timeStart = System.currentTimeMillis();
	    HttpResponse response = httpclient.execute(httppost);
	    long endTime = System.currentTimeMillis();
	    System.out.println("Completed in " + String.valueOf(endTime-timeStart) +"ms");
	    HttpEntity resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    
	    ///////////////////Get Delivery Data
	    
	    HttpGet httpget = new HttpGet("https://delivery.bloomnet.net/DeliveryAPI/v1/RetrieveDeliveryInfoByOrderNumber?apiKey=mf$Sf3m!tkdsZkf5nP(7dnsD$1co9DGjNsf2niCn$0vUf(I9em&orderNumber=SPIDERMAN2828");
	    
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    timeStart = System.currentTimeMillis();
	    response = httpclient.execute(httpget);
	    endTime = System.currentTimeMillis();
	    System.out.println("Completed in " + String.valueOf(endTime-timeStart) +"ms");
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    
	    httpget = new HttpGet("https://delivery.bloomnet.net/DeliveryAPI/v1/RetrieveDeliveryInfoByOrderNumber?apiKey=mf$Sf3m!tkdsZkf5nP(7dnsD$1co9DGjNsf2niCn$0vUf(I9em&orderNumber=SPIDERMAN2949");
	    
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    timeStart = System.currentTimeMillis();
	      response = httpclient.execute(httpget);
	     endTime = System.currentTimeMillis();
	    System.out.println("Completed in " + String.valueOf(endTime-timeStart) +"ms");
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    
	    httpclient.close();
	  }
}
